const switchThemes = () => {
    const switchThemeButton = document.querySelector(".theme-switcher-dark");
    const body = document.body;
    let storage = localStorage;

    switchThemeButton.addEventListener("click", () => {
        if (switchThemeButton.classList.contains("theme-switcher-dark")) {
            switchThemeButton.classList.remove("theme-switcher-dark");
            switchThemeButton.classList.add("theme-switcher-bright");
            body.classList.add("bright-theme");
            storage.removeItem("theme");
        } else {
            switchThemeButton.classList.remove("theme-switcher-bright");
            switchThemeButton.classList.add("theme-switcher-dark");
            body.classList.remove("bright-theme");
            storage.setItem("theme", body.className);
        }
    });

    window.onload = () => {
        const theme = JSON.parse(localStorage.getItem("theme"));
        if (theme !== "bright-theme") {
            body.classList.remove("dark-theme");
            body.classList.add("bright-theme");
            switchThemeButton.classList.remove("theme-switcher-dark");
            switchThemeButton.classList.add("theme-switcher-bright");
        }
    };
};

switchThemes();
